import { Component, NgModule } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'meteo'; 
  constructor(private montitle: Title, private meta: Meta) {
    this.montitle.setTitle('meteo');
    this.meta.addTag({name:'Philippe', content: 'Mon site'});
  }
}
