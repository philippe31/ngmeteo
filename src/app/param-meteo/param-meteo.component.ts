import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ParamsService } from '../services/params.service';

import { from } from 'rxjs';
import { ParamMeteo } from '../interfaces/param-meteo';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-param-meteo',
  templateUrl: './param-meteo.component.html',
  styleUrls: ['./param-meteo.component.scss']
})
export class ParamMeteoComponent implements OnInit, ParamMeteo {
  paramForm: FormGroup;
  submitted = false;
  hours = [];

  heure: string;
  icon: boolean;
  condition: boolean;
  temperature: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private paramsService: ParamsService,
    private router: Router,
    private title: Title
  ) {
    this.title.setTitle('Parametres');
    for (let i = 0; i < 24; i++) {
      this.hours.push(i + 'H00');
    }
  }

  ngOnInit() {
    this.paramForm = this.formBuilder.group({
      heure: ['', Validators.required],
      icon: [false],
      condition: [false],
      temperature:[false]
    });
    this.heure = this.paramsService.getHeure();
    this.icon = this.paramsService.getIcon();
    this.condition = this.paramsService.getCondition();
    this.temperature = this.paramsService.getTemperature();
  }

  onSubmit() {
    this.submitted = true;
    if (this.paramForm.invalid) {
      return;
    }
    this.heure = this.paramForm.get('heure')
      ? this.paramForm.get('heure').value
      : null;
    this.paramsService.setHeure(this.heure);

    this.icon = this.paramForm.get('icon')
      ? this.paramForm.get('icon').value
      : null;
    this.paramsService.setIcon(this.icon);

    this.condition = this.paramForm.get('condition')
      ? this.paramForm.get('condition').value
      : null;
    this.paramsService.setCondition(this.condition);

    this.temperature = this.paramForm.get('temperature')
      ? this.paramForm.get('temperature').value
      : null;
    this.paramsService.setTemperature(this.temperature);

    this.router.navigate([`/meteo`]);

    //const heure = this.paramForm.get('heure');
    //console.log(heure);
    /*     this.router.navigated = false;
    this.router.navigate([`/meteo/${ville}`]); */
    /* 
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.navigate([`/meteo/${ville}`]); */
  }

  onReset() {
    this.submitted = false;
    //this.paramForm.reset;
  }
}
