import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { LaregionService } from '../services/laregion.service';
import * as L from 'leaflet';

@Component({
  selector: 'app-aeroport',
  templateUrl: './aeroport.component.html',
  styleUrls: ['./aeroport.component.scss'],
})
export class AeroportComponent implements OnInit {
  constructor(private title: Title, private serviceLaRegion: LaregionService) {
    this.title.setTitle('Aéroports');
  }

  ngOnInit() {
    const myMap = L.map('mymap').setView([43.625129, 1.485055], 8);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: 'AFPA Map',
    }).addTo(myMap);

    const myIcon = L.icon({
      iconUrl: 'https://philippe.afpa-balma.fr/ng/images/airport-32x32.png',
    });

    this.serviceLaRegion.getAeroports().subscribe(
      (data: any) => {
        data.records.forEach((lesDonnees: any) => {
          console.log(lesDonnees);
          let latlng: any = [
            lesDonnees.geometry.coordinates[1],
            lesDonnees.geometry.coordinates[0],
          ];
          L.marker(latlng, { icon: myIcon })
            .addTo(myMap)
            .bindPopup(lesDonnees.fields.nom);
        });
      },
      (error: any) => console.log('Erreur: ' + error),
      () => console.log('fini')
    );
  }
}
