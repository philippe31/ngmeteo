import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { VilleService } from '../services/ville.service';
import { Ville } from '../interfaces/ville';
import { MeteoService } from '../services/meteo.service';
import { Meteo } from '../interfaces/meteo';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-villes',
  templateUrl: './villes.component.html',
  styleUrls: ['./villes.component.scss']
})
export class VillesComponent implements OnInit {

  @Input() villes: Ville[];
  @Output() villeSelected = new EventEmitter<Ville>();

  ville: Observable<Ville>;

  // villes: Ville[];
  constructor(private villeService: VilleService) { }

  ngOnInit() {
    this.villes = this.villeService.getVilles();
    // this.meteoService.getMeteo('toulouse').then((data)=>(this.meteo = data));
  }

  selectVille(i: number) {
    console.log(this.villes[i]);
    this.villeSelected.emit(this.villes[i]);
  }

}
