import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MeteoComponent } from './meteo/meteo.component';
import { VilleComponent } from './ville/ville.component';
import { VillesComponent } from './villes/villes.component';
import { ManifestationsCulturellesComponent } from './manifestations-culturelles/manifestations-culturelles.component';
import { ParamMeteoComponent } from './param-meteo/param-meteo.component';
import { RecetteComponent } from './recette/recette.component';
import { RadarComponent } from './radar/radar.component';
import { AeroportComponent } from './aeroport/aeroport.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'meteo', component: MeteoComponent },
  { path: 'meteo/:id', component: MeteoComponent},
  { path: 'ville', component: VilleComponent},
  { path: 'recette', component: RecetteComponent},
  { path: 'recette/:id', component: RecetteComponent},
  // { path: 'villes', component: VillesComponent},
  { path: 'param-meteo', component: ParamMeteoComponent},
  { path: 'manifestations-culturelles', component: ManifestationsCulturellesComponent},
  { path: 'radar', component: RadarComponent},
  { path: 'aeroport', component: AeroportComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
