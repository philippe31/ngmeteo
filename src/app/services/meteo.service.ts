import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meteo } from '../interfaces/meteo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeteoService {
  protected url = 'https://www.prevision-meteo.ch/services/json/';
  private ville: string = "toulouse";


  /*  meteo: Meteo; */

  constructor(private http: HttpClient) {}

  public getMeteo(): Observable<any> {
    return this.http.get(`${this.url}${this.ville}`);
  }

  public setVille(ville: string) {
    this.ville = ville;
  }

  public getVille() {
    return this.ville;
  }
}
