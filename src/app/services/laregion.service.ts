import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LaregionService {
  private urlLaRegion: string =
    'https://data.laregion.fr/api/records/1.0/search/?';
  private aeroports: string = 'dataset=aeroports-occitanie&q=&facet=nom';
  private radars: string =
    'dataset=radars-en-occitanie&q=&facet=name&facet=department';

  constructor(private http: HttpClient) {}

  public getRadars() {
    return this.http.get(this.urlLaRegion + this.radars);
  }

  public getAeroports() {
    return this.http.get(this.urlLaRegion + this.aeroports);
  }

}
