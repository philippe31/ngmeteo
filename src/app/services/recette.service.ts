import { Injectable } from '@angular/core';
import { Recette, Ingredient, Etape } from '../interfaces/recette';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecetteService implements Recette {
  soustitre: string;
  titre: string;
  ingredients: Ingredient[];
  etapes: Etape[];

  protected url = './assets/data/recettes.json';

  constructor(private http: HttpClient) {}

  public getRecette(): Observable<any> {
    return this.http.get(`${this.url}`);
  }
}
