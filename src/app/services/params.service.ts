import { Injectable } from '@angular/core';
import { ParamMeteo } from '../interfaces/param-meteo';

@Injectable({
  providedIn: 'root'
})
export class ParamsService implements ParamMeteo {
  heure: string;

  condition: boolean;
  temperature: boolean;
  icon: boolean;

  constructor() {
    const date: Date = new Date();
    this.heure = date.getHours() + 'H00';
    this.icon = true;
    this.condition = true;
    this.temperature = true;
  }

  getHeure(): string {
    return this.heure;
  }
  setHeure(h: string) {
    this.heure = h;
  }

  getIcon(): boolean {
    return this.icon;
  }
  setIcon(icon: boolean) {
    this.icon = icon;
  }

  getCondition(): boolean {
    return this.condition;
  }
  setCondition(condition: boolean) {
    this.condition = condition;
  }

  getTemperature(): boolean {
    return this.temperature;
  }
  setTemperature(temperature: boolean) {
    this.temperature = temperature;
  }
}
