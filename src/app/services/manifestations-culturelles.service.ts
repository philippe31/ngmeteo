import { Injectable } from '@angular/core';
import { Observable, bindNodeCallback, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, switchMap, catchError } from 'rxjs/operators';
import { xml2js } from 'xml2js';
import { NewsRss } from '../interfaces/news-rss';

@Injectable({
  providedIn: 'root'
})
export class ManifestationsCulturellesService {
  RssData: NewsRss;
  protected url =
    'https://data.toulouse-metropole.fr/api/records/1.0/search/?dataset=agenda-des-manifestations-culturelles-so-toulouse&facet=type_de_manifestation';

  //protected urlLEquipe = 'https://rmcsport.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/';

  protected urlLEquipe =
    'http://localhost/ajax/ajax-xml/get-ajax-xml.php?url=https://rmcsport.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/';

    protected urlRss = "https://www.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/";

  constructor(private http: HttpClient) {}

  public getManifestationsCulturelles(): Observable<any> {
    return this.http.get(this.url).pipe(map((data: any) => data.records));
  }

  public getLEquipe(): Observable<any> {
    /*     const headers = new HttpHeaders({ 'Content-Type': 'text/xml' });
    headers.append('Accept', 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'GET');
    headers.append('Access-Control-Allow-Origin', '*'); */

    /*     const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers':
        'authorization, origin, x-requested-with, accept, content-type, user-agent,',
      'Access-Control-Allow-Methods': 'GET'
    }); */

    /*     return this.http
      .get(this.urlLEquipe, { headers })
      .pipe(map((data: any) => data)); */

    /*       console.log(this.urlLEquipe);
    return this.http.get<any>(this.urlLEquipe); */

    /* return this.http
      .get(this.urlLEquipe)
      .pipe(switchMap(res => bindNodeCallback(xml2js.parseString)(res))); */

    //return this.http.get(this.urlLEquipe);

    /*     const requestOptions: Object = {
      observe: "body",
      responseType: "text"
    }; */
    console.log('getLEquipe()');
/*     const headers = new HttpHeaders();
    headers.append('Accept', 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'GET');
    headers.append('Access-Control-Allow-Origin', '*'); */
    return this.http.get(this.urlRss).pipe(
      map((data: any) => {
        //return data.json();
        data;
      }),
      catchError(error =>{
        return throwError('Erreur service getLEquipe');
      })
    );
  }
}
