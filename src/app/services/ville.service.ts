import { Injectable } from '@angular/core';
import { Ville } from '../interfaces/ville';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VilleService {

  mesVilles: Ville[] = [
    {nom: 'Toulouse', code: 'toulouse', cp: 31000},
    {nom: 'Ramonville Saint Agne', code: 'ramonville-saint-agne', cp: 31520}
  ];
  constructor() { }

  getVilles(): Ville[] {
    return this.mesVilles;
  }

  getVille(i: number): Ville {
    return this.mesVilles[i];
  }

}
