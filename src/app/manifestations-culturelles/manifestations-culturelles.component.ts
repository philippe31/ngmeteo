import { Component, OnInit } from '@angular/core';
import { ManifestationsCulturellesService } from '../services/manifestations-culturelles.service';
import { map } from 'rxjs/operators';
import { NewsRss } from '../interfaces/news-rss';
import { xml2js } from 'xml2js';
import { Title } from '@angular/platform-browser';
import { typeWithParameters } from '@angular/compiler/src/render3/util';

@Component({
  selector: 'app-manifestations-culturelles',
  templateUrl: './manifestations-culturelles.component.html',
  styleUrls: [
    './manifestations-culturelles.component.scss',
    '../../assets/scss/loader.scss',
  ],
})
export class ManifestationsCulturellesComponent implements OnInit {
  manifestations: any;
  lequipe: any;
  errors: any;
  RssData: NewsRss;
  constructor(
    private manifestationsCulturellesService: ManifestationsCulturellesService,
    private title: Title
  ) {
    this.title.setTitle('Culture');
  }

  ngOnInit() {
    this.manifestationsCulturelles();
    //this.equipe();
  }

  manifestationsCulturelles() {
    this.manifestationsCulturellesService
      .getManifestationsCulturelles()
      .subscribe(
        (data: any) => {
          // console.log(data);
          if (!data) {
            this.errors = data;
          } else {
            this.manifestations = data;
          }
        },
        (err: any) => {
          console.log('Erreur' + err);
        }
      );
  }

  equipe() {
    /*     this.manifestationsCulturellesService
    .getLEquipe()
    .subscribe(
      (data: any) => {
        console.log(data);
        if (!data) {
          this.errors = data;
        } else {
          this.lequipe = data;
        }
      },
      (data: any) => {
        console.log("Erreur" + data);
      }
    ); */

    /**
     * return this.http
      .get("https://gadgets.ndtv.com/rss/feeds", requestOptions)
      .subscribe(data => {
        let parseString = xml2js.parseString;
        parseString(data, (err, result: NewsRss) => {
          this.RssData = result;
        });
      });
     */

    this.manifestationsCulturellesService
      .getLEquipe()
      .subscribe((data: any) => {
        console.log('ici');
        console.log(data);
        Object.values(data['items']).map((item) => {
          console.log(item);
        });
        //this.lequipe = data;
        //console.log(data);

        /*       xml2js.parseString(data, (err, result: NewsRss) => {
        if (err) {
          console.log('erreur');
        } else {
          this.RssData = result;
        }
      }); */
        //let parseString = xml2js.parseString;
        //parseString(data, (err, result: NewsRss) => {
        //  this.RssData = result;
        //});
      });
    /* .subscribe(
      (data: any) => {
        console.log(data);
        if (!data) {
          this.errors = data;
        } else {
          this.lequipe = data;
        }
      },
      (data: any) => {
        console.log("Erreur" + data);
      }
    ); */
  }
}
