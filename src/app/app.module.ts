import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { VilleComponent } from './ville/ville.component';
import { VillesComponent } from './villes/villes.component';
import { HomeComponent } from './home/home.component';
import { MeteoComponent } from './meteo/meteo.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManifestationsCulturellesComponent } from './manifestations-culturelles/manifestations-culturelles.component';
import { ReplacePipe } from './pipes/replace.pipe';
import { NgModule } from '@angular/core';
import { ParamMeteoComponent } from './param-meteo/param-meteo.component';
import { RecetteComponent } from './recette/recette.component';
import { RadarComponent } from './radar/radar.component';
import { AeroportComponent } from './aeroport/aeroport.component';
import { WidgetMeteoComponent } from './widget-meteo/widget-meteo.component';
import { MeteoService } from './services/meteo.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    VilleComponent,
    VillesComponent,
    HomeComponent,
    MeteoComponent,
    ManifestationsCulturellesComponent,
    ReplacePipe,
    ParamMeteoComponent,
    RecetteComponent,
    RadarComponent,
    AeroportComponent,
    WidgetMeteoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'})
  ],
  providers: [MeteoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
