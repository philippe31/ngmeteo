import { Component, OnInit } from '@angular/core';
import { MeteoService } from '../services/meteo.service';
import { Meteo } from '../interfaces/meteo';
import { ActivatedRoute } from '@angular/router';
import { ParamsService } from '../services/params.service';
import { title } from 'process';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-meteo',
  templateUrl: './meteo.component.html',
  styleUrls: ['./meteo.component.scss', '../../assets/scss/loader.scss']
})
export class MeteoComponent implements OnInit {
  meteo: Meteo;
  errors: any;
  ville = 'toulouse';
  previsionnel: boolean;
  classBouton: string = "btn btn-default";
  classSpan = {
    true: "glyphicon glyphicon-menu-up",
    false: "glyphicon glyphicon-menu-down"
  };

  temperature: boolean;
  condition: boolean;
  icon: boolean;

  constructor(
    private meteoService: MeteoService,
    private paramsService: ParamsService,

    private route: ActivatedRoute,
    private title: Title
  ) {
    this.title.setTitle('Météo');

    this.temperature = this.paramsService.getTemperature();
    this.condition = this.paramsService.getCondition();
    this.icon = this.paramsService.getIcon();

  }

  ngOnInit() {
    // console.log(this.route.snapshot.paramMap);
    const id = this.route.snapshot.paramMap.get('id');
    this.ville = id ? id : this.meteoService.getVille() ? '' : this.ville;
    this.getMeteo(this.ville);
    this.previsionnel = true;
  }

  getMeteo(ville: string = '') {
    console.log('1/ ' + ville, this.meteoService.getVille());
    // tslint:disable-next-line: triple-equals
    if (ville != this.meteoService.getVille() || this.ville == '') {
      console.log('2/ ' + ville, this.meteoService.getVille());
      this.meteoService.setVille(
        // tslint:disable-next-line: triple-equals
        ville != '' ? ville : this.meteoService.getVille()
      );
      this.meteoService.getMeteo().subscribe(
        (data: any) => {
          if (data.errors) {
            this.errors = data;
          } else {
            this.meteo = data;
          //   const {fcst_day_0:{hourly_data}} = data;
          //   console.log(hourly_data);
            
          // let { fcst_day_0 : o } = data; 
          // console.log(o);
          }
        },
        (error: any) => {
          console.log('Erreur' + error);
        }
      );
    }
  }

  tooglePrevisionnel(){
    this.previsionnel = !this.previsionnel;
  }
}
