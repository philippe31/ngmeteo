import { Component, OnInit } from '@angular/core';
import { RecetteService } from '../services/recette.service';
import { Recette, Recettes } from '../interfaces/recette';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-recette',
  templateUrl: './recette.component.html',
  styleUrls: ['./recette.component.scss', '../../assets/scss/loader.scss']
})
export class RecetteComponent implements OnInit {
  errors: any;
  recette: Recette;
  recettes: Recettes;
  liste: boolean;

  constructor(
    private recetteService: RecetteService,
    private route: ActivatedRoute,
    private title : Title
  ) {
    // this.getRecette(1);
    this.title.setTitle('Recettes');
  }

  ngOnInit() {
    
    const id = this.route.snapshot.paramMap.get('id')
      ? +this.route.snapshot.paramMap.get('id')
      : -1;

    if (id >= 0) {
      this.liste = false;
      this.getRecette(id);
    } else {
      this.liste = true;
      this.getRecettes();
    }
  }

  getRecettes(): void {
    this.recetteService.getRecette().subscribe(
      (data: any) => {
        if (data.errors) {
          this.errors = data;
        } else {
          this.recettes = data;
        }
      },
      (error: any) => {
        console.log('Erreur' + error);
      }
    );
  }

  getRecette(id: number) {
    this.recetteService.getRecette().subscribe(
      (data: any) => {
        // console.log(data[1]);
        if (data.errors) {
          this.errors = data;
        } else {
          this.recette = data[id];
        }
      },
      (error: any) => {
        console.log('Erreur' + error);
      }
    );
  }
}
