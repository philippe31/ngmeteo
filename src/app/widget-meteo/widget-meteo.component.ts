import { Component, OnInit } from '@angular/core';
import { MeteoService } from '../services/meteo.service';

@Component({
  selector: 'app-widget-meteo',
  templateUrl: './widget-meteo.component.html',
  styleUrls: ['./widget-meteo.component.scss']
})
export class WidgetMeteoComponent implements OnInit {

  private donnees: any;
  private errors: any;

  constructor(private meteoService:MeteoService) { }

  ngOnInit() {
    this.getMeteo('toulouse');
    
  }
  
  getMeteo(ville: string = '') {

      this.meteoService.getMeteo().subscribe(
        (data: any) => {
          console.log(data);
          if (data.errors) {
            this.errors = data;
          } else {
            this.donnees = data;
          //   const {fcst_day_0:{hourly_data}} = data;
          //   console.log(hourly_data);
            
          // let { fcst_day_0 : o } = data; 
          // console.log(o);
          }
        },
        (error: any) => {
          console.log('Erreur' + error);
        },
        () => console.log("c'est fini")
      )

  }

}
