export interface Ingredient {
  libelle: string;
  quantite: number;
  unite: string;
}

export interface Etape {
  texte: string;
}

export interface Recette {
  titre: string;
  soustitre: string;
  ingredients: Array<Ingredient>;
  etapes: Array<Etape>;
}

export interface Recettes {
  titre: string;
  soustitre: string;
}
