export interface Ville {
    nom: string;
    code?: string;
    cp?: number;
}
