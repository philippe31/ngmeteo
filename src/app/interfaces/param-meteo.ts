export interface ParamMeteo {
    heure: string;
    icon: boolean;
    condition: boolean;
    temperature: boolean;
}
