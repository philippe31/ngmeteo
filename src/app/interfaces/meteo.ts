export interface Meteo {
    city_info: {
        name: string
    };
    current_condition: {
        date: Date,
        condition: string,
        tmp: string,
        icon_big: string
    };
    fcst_day_0: {};
    fcst_day_1: {};
    fcst_day_2: {};
    fcst_day_3: {};
    fcst_day_4: {};
    forecast_info: {};
}
