import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replace'
})
export class ReplacePipe implements PipeTransform {
  transform(value: string, ...args: any[]): string {
    if (!value || !args[0] || !args[1]) {
      return value;
    }
    return value.replace(new RegExp(args[0], 'g'), args[1]);
  }
}
