import { Component, OnInit } from '@angular/core';
import { MeteoService } from '../services/meteo.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ville',
  templateUrl: './ville.component.html',
  styleUrls: ['./ville.component.scss']
})
export class VilleComponent implements OnInit {
  villeForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder, private router: Router) {
    this.villeForm = this.formBuilder.group({
      ville: ['', [Validators.required, Validators.pattern(/[A-Za-z]{3,}/)]]
    });
  }

  ngOnInit() {}

  onSubmit() {
    const ville = this.villeForm.get('ville').value;
    // console.log(ville);
    /*     this.router.navigated = false;
    this.router.navigate([`/meteo/${ville}`]); */

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.navigate([`/meteo/${ville}`]);
  }
}
