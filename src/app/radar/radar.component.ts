import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { Title } from '@angular/platform-browser';
import { LaregionService } from '../services/laregion.service';

@Component({
  selector: 'app-radar',
  templateUrl: './radar.component.html',
  styleUrls: ['./radar.component.scss'],
})
export class RadarComponent implements OnInit {
  constructor(private title: Title, private serviceLaRegion: LaregionService) {
    this.title.setTitle('Radars');
  }

  ngOnInit() {
   this.afficher();
  }

  afficher(){
    const myMap = L.map('mymap').setView([43.625129, 1.485055], 8);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: 'AFPA Map',
    }).addTo(myMap);

    const myIcon = L.icon({
      iconUrl: 'https://philippe.afpa-balma.fr/ng/images/radar-32x32.png',
    });

    this.serviceLaRegion.getRadars().subscribe(
      (data: any) => {
        data.records.forEach((lesDonnees: any) => {
          if(lesDonnees.geometry != undefined){
            let latlng: any = [
              lesDonnees.geometry.coordinates[1],
              lesDonnees.geometry.coordinates[0],
            ];
            L.marker(latlng, { icon: myIcon })
              .addTo(myMap)
              .bindPopup(lesDonnees.fields.radardirection);
          }
        });
      },
      (error: any) => console.log('Erreur: ' + error),
      () => console.log('fini')
    );
  }
}
